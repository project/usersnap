<?php

namespace Drupal\usersnap\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form for Usersnap widget.
 */
class UsersnapSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'usersnap_admin_configuration';
  }

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return ['usersnap.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $admin_configurations = $this->config('usersnap.settings');
    $form['usersnap_apikey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter the Usersnap API Key'),
      '#default_value' => $admin_configurations->get('usersnap_apikey') ? $admin_configurations->get('usersnap_apikey') : '',
      '#size' => 60,
      '#maxlength' => 60,
      '#description' => $this->t("Enter the Usersnap API Key. You can get a Key at http://www.usersnap.com."),
      '#required' => TRUE,
    ];
    $form['usersnap_visibility'] = [
      '#type' => 'radios',
      '#title' => $this->t('Show widget on specific pages'),
      '#default_value' => $admin_configurations->get('usersnap_visibility') ? $admin_configurations->get('usersnap_visibility') : 0,
      '#options' => [
        0 => $this->t('All pages except those listed'),
        1 => $this->t('Only the listed pages'),
      ],
    ];
    $form['usersnap_paths'] = [
      '#type' => 'textarea',
      '#title' => '',
      '#description' => $this->t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", [
        '%blog' => 'blog',
        '%blog-wildcard' => 'blog/*',
        '%front' => '<front>',
      ]
      ),
      '#default_value' => $admin_configurations->get('usersnap_paths') ? $admin_configurations->get('usersnap_paths') : '',
    ];
    $form['usersnap_language'] = [
      '#type' => 'select',
      '#title' => $this->t('Widget language'),
      '#description' => $this->t('Preferred interface language for feedback widget.'),
      '#default_value' => $admin_configurations->get('usersnap_apikey') ? $admin_configurations->get('usersnap_apikey') : 'en',
      '#options' => $this->usersnapSupportedLanguages(),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config_values = $form_state->getValues();
    $config_fields = [
      'usersnap_apikey',
      'usersnap_visibility',
      'usersnap_paths',
      'usersnap_language',
    ];
    $usersnap_config = $this->config('usersnap.settings');
    foreach ($config_fields as $config_field) {
      $usersnap_config->set($config_field, $config_values[$config_field])
        ->save();
    }
    parent::submitForm($form, $form_state);
  }

  /**
   * Helper function to return Usersnap supported languages and language codes.
   *
   * @see https://usersnap.com/docs#language
   */
  private function usersnapSupportedLanguages() {
    return [
      'en' => $this->t('English'),
      'de-informal' => $this->t('German (informal)'),
      'de-formal' => $this->t('German (formal)'),
      'fr' => $this->t('French'),
      'es' => $this->t('Spanish'),
      'pl' => $this->t('Polish'),
      'fa' => $this->t('Farsi'),
      'is' => $this->t('Icelandic'),
      'jp' => $this->t('Japanese'),
      'it' => $this->t('Italian'),
      'hu' => $this->t('Hungarian'),
      'ko' => $this->t('Korean'),
      'cz' => $this->t('Czech'),
      'da' => $this->t('Danish'),
      'no' => $this->t('Norwegian'),
      'sk' => $this->t('Slovakian'),
      'nl' => $this->t('Dutch'),
      'sv' => $this->t('Swedish'),
      'pt' => $this->t('Portuguese'),
      'fi' => $this->t('Finnish'),
      'ru' => $this->t('Russian'),
      'tr' => $this->t('Turkish'),
    ];
  }

}

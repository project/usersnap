/**
 * @file Usersnap Widget load.
 *
 * @module usersnap.
 */
(function (drupalSettings) {
  // Usersnap was loaded, the callback is set as part of the embed.
  // @see usersnap_library_info_build()
  window.onUsersnapLoad = function (api) {
    api.init({
      locale: drupalSettings.usersnap.language,
      custom: {
        'Drupal user email': drupalSettings.usersnap.user_email,
        'Drupal user name': drupalSettings.usersnap.user_name,
        'Drupal user id': drupalSettings.usersnap.user_id,
      }
    });

    api.on('open', function(event) {
      event.api.setValue('visitor', drupalSettings.usersnap.user_email);
    });
  };
})(drupalSettings);

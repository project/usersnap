Usersnap
========

This Drupal module integrates the [Usersnap](usersnap.com) feedback widget with your Drupal
website. Before you install, be sure you have a Usersnap account and API key.


### Installation instructions

Install and enable this module [like you would any other module][]. Once you've
successfully installed the module, navigate to the configuration page at
`/admin/config/services/usersnap`. There, you must enter your Usersnap API key.

This module also exposes an "access usersnap" permission, which you can grant to
specific roles that you want to have access to the Usersnap feedback widget. You
may even wish to grant this permission to the "anonymous user" role to allow any
website visitor to send feedback through Usersnap.
